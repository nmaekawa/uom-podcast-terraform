#!/bin/sh

cat > mkdocs.yml <<EOF
---
site_name: "Podcast Service: Terraform"
repo_url: "https://bitbucket.org/stuartphillipson/podcast-terraform/"
nav:
  - Home: "README.md"
  - Modules:
EOF

rm -rf docs site
mkdir docs
ln -s ../README.md docs/README.md

for d in $(ls -1 modules)
do
  echo "Creating $d module docs"
  terraform-docs md modules/$d > modules/$d/README.md
  ln -s ../modules/$d/README.md docs/${d}.md
  echo "    - $d: \"${d}.md\"" >> mkdocs.yml
done

mkdocs build



