#!/bin/sh

env=$1

terraform workspace select $env

rm -f terraform.tfvars
ln -s $env.tfvars terraform.tfvars
