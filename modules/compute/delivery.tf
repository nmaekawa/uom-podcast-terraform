# compute/delivery

# Video Portal and Tools

#
# Video
#
resource "aws_instance" "video" {
  count         = var.video_count
  ami           = data.aws_ami.centos.id
  instance_type = var.video_type
  subnet_id     = element(var.subnet_ids.delivery, count.index)
  key_name      = local.key_access_name
  root_block_device {
    delete_on_termination = true
  }
  vpc_security_group_ids = [var.security_group_ids.default]
  iam_instance_profile   = var.profile_names.video
  tags = {
    Name = "${var.prefix}-video-${count.index}"
    Env  = var.env
    App  = "videolounge"
    host = "video"
  }
}

#
# Tools
#
resource "aws_instance" "tools" {
  count         = var.tools_count
  ami           = data.aws_ami.centos.id
  instance_type = var.tools_type
  subnet_id     = element(var.subnet_ids.delivery, count.index)
  key_name      = local.key_access_name
  root_block_device {
    delete_on_termination = true
  }
  vpc_security_group_ids = [var.security_group_ids.default]
  iam_instance_profile   = var.profile_names.tools
  user_data              = data.template_file.primary.rendered
  tags = {
    Name = "${var.prefix}-tools-${count.index}"
    Env  = var.env
    App  = "podcast tools"
    host = "tools"
  }
}

