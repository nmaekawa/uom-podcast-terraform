# compute/processing.tf
# Opencast node instances

#
# Admin
#
resource "aws_instance" "admin" {
  count         = var.admin_standby ? 2 : 1
  ami           = data.aws_ami.centos.id
  instance_type = var.admin_type
  subnet_id     = element(var.subnet_ids.processing, count.index)
  key_name      = local.key_access_name
  root_block_device {
    delete_on_termination = true
  }
  vpc_security_group_ids = [var.security_group_ids.default]
  iam_instance_profile   = var.profile_names.admin
  user_data              = count.index == 0 ? data.template_file.opencast_primary.rendered : data.template_file.opencast_standby.rendered
  tags = {
    Name      = "${var.prefix}-admin${count.index == 0 ? "" : "-standby"}"
    Env       = var.env
    App       = "opencast"
    Component = "administration"
    host      = "admin"
  }
}

resource "aws_volume_attachment" "admin_index" {
  device_name = "/dev/sdb"
  volume_id   = var.ebs_opencast_elasticsearch_id
  instance_id = aws_instance.admin[var.primary_zone_index].id
}

resource "aws_volume_attachment" "admin_archive" {
  device_name = "/dev/sdc"
  volume_id   = var.ebs_opencast_archive_id
  instance_id = aws_instance.admin[var.primary_zone_index].id
}

#
# Edit
#
resource "aws_instance" "edit" {
  count         = var.edit_count
  ami           = data.aws_ami.centos.id
  instance_type = var.edit_type
  subnet_id     = element(var.subnet_ids.processing, count.index)
  key_name      = local.key_access_name
  root_block_device {
    delete_on_termination = true
  }
  vpc_security_group_ids = [var.security_group_ids.default]
  iam_instance_profile   = var.profile_names.publication
  user_data              = data.template_file.opencast_primary.rendered
  tags = {
    Name      = "${var.prefix}-edit-${count.index}"
    Env       = var.env
    App       = "opencast"
    Component = "editing"
    host      = "edit"
  }
}

#
# Upload Scaling Group
#
resource "aws_launch_configuration" "upload" {
  name                 = "${var.prefix}-upload-${local.upload_ami_id}"
  image_id             = local.upload_ami_id
  instance_type        = var.upload_type
  key_name             = local.key_access_name
  security_groups      = [var.security_group_ids.default]
  user_data            = data.template_file.opencast_primary.rendered
  iam_instance_profile = var.profile_names.upload
  root_block_device {
    delete_on_termination = true
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "null_resource" "tag_upload_ami" {
  triggers = {
    upload_ami_id = local.upload_ami_id
  }
  provisioner "local-exec" {
    environment = {
      ami_name_pattern = "${var.prefix}-opencast-upload"
      ami_in_use_id = "${local.upload_ami_id}"
    }
    command = "${path.module}/scripts/tag_in_use_ami.sh"
  }
}

resource "aws_autoscaling_group" "upload" {
  name                 = "${var.prefix}-upload"
  max_size             = var.autoscale_upload_max
  min_size             = var.autoscale_upload_min
  desired_capacity     = var.autoscale_upload_desired
  launch_configuration = aws_launch_configuration.upload.name
  vpc_zone_identifier  = var.subnet_ids.processing
  target_group_arns    = [aws_alb_target_group.upload.arn, aws_alb_target_group.upload_internal.arn]
  tags = [
    {
      key                 = "Name"
      value               = "${var.prefix}-upload"
      propagate_at_launch = true
    },
    {
      key                 = "Env"
      value               = var.env
      propagate_at_launch = true
    },
    {
      key                 = "App"
      value               = "opencast"
      propagate_at_launch = true
    },
    {
      key                 = "host"
      value               = "upload"
      propagate_at_launch = true
    },
  ]
}

resource "aws_autoscaling_lifecycle_hook" "upload_terminate" {
  name                   = "terminate"
  autoscaling_group_name = aws_autoscaling_group.upload.name
  default_result         = "CONTINUE"
  heartbeat_timeout      = local.autoscale_upload_heartbeat
  lifecycle_transition   = "autoscaling:EC2_INSTANCE_TERMINATING"
}

resource "aws_autoscaling_policy" "upload" {
  name                   = "network-in"
  autoscaling_group_name = aws_autoscaling_group.upload.name
  policy_type            = "TargetTrackingScaling"
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageNetworkIn"
    }
    target_value = var.autoscale_upload_network_in_target
  }
}

resource "aws_autoscaling_schedule" "upload_scale_in" {
  scheduled_action_name  = "scale-in-weekday"
  autoscaling_group_name = aws_autoscaling_group.upload.name
  max_size               = var.autoscale_upload_quiet_max
  min_size               = var.autoscale_upload_quiet_min
  desired_capacity       = var.autoscale_upload_quiet_desired
  recurrence             = "0 18 * * MON-FRI"
}

resource "aws_autoscaling_schedule" "upload_scale_out" {
  scheduled_action_name  = "scale-out-weekday"
  autoscaling_group_name = aws_autoscaling_group.upload.name
  max_size               = var.autoscale_upload_max
  min_size               = var.autoscale_upload_min
  desired_capacity       = var.autoscale_upload_desired
  recurrence             = "0 09 * * MON-FRI"
}

#
# Work Scaling Group
#
resource "aws_launch_configuration" "work" {
  name                 = "${var.prefix}-work-${local.work_ami_id}"
  image_id             = local.work_ami_id
  instance_type        = var.work_type
  key_name             = local.key_access_name
  security_groups      = [var.security_group_ids.default]
  user_data            = data.template_file.opencast_primary.rendered
  iam_instance_profile = var.profile_names.work
  root_block_device {
    delete_on_termination = true
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "null_resource" "tag_work_ami" {
  triggers = {
    work_ami_id = local.work_ami_id
  }
  provisioner "local-exec" {
    environment = {
      ami_name_pattern = "${var.prefix}-opencast-worker"
      ami_in_use_id = "${local.work_ami_id}"
    }
    command = "${path.module}/scripts/tag_in_use_ami.sh"
  }
}

resource "aws_autoscaling_group" "work" {
  name                 = "${var.prefix}-work"
  max_size             = var.autoscale_work_max
  min_size             = var.autoscale_work_min
  desired_capacity     = var.autoscale_work_desired
  launch_configuration = aws_launch_configuration.work.name
  vpc_zone_identifier  = var.subnet_ids.processing

  tags = [
    {
      key                 = "Name"
      value               = "${var.prefix}-work"
      propagate_at_launch = true
    },
    {
      key                 = "Env"
      value               = var.env
      propagate_at_launch = true
    },
    {
      key                 = "App"
      value               = "opencast"
      propagate_at_launch = true
    },
    {
      key                 = "host"
      value               = "work"
      propagate_at_launch = true
    },
  ]
}

resource "aws_autoscaling_policy" "work_cpu" {
  name                   = "cpu"
  autoscaling_group_name = aws_autoscaling_group.work.name
  policy_type            = "TargetTrackingScaling"
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = var.autoscale_work_cpu_target
  }
}

resource "aws_autoscaling_lifecycle_hook" "work_terminate" {
  name                   = "terminate"
  autoscaling_group_name = aws_autoscaling_group.work.name
  default_result         = "CONTINUE"
  heartbeat_timeout      = local.autoscale_work_heartbeat
  lifecycle_transition   = "autoscaling:EC2_INSTANCE_TERMINATING"
}

resource "aws_autoscaling_schedule" "work_scale_in" {
  scheduled_action_name  = "scale-in-weekday"
  autoscaling_group_name = aws_autoscaling_group.work.name
  max_size               = var.autoscale_work_quiet_max
  min_size               = var.autoscale_work_quiet_min
  desired_capacity       = var.autoscale_work_quiet_desired
  recurrence             = "0 18 * * MON-FRI"
}

resource "aws_autoscaling_schedule" "work_scale_out" {
  scheduled_action_name  = "scale-out-weekday"
  autoscaling_group_name = aws_autoscaling_group.work.name
  max_size               = var.autoscale_work_max
  min_size               = var.autoscale_work_min
  desired_capacity       = var.autoscale_work_desired
  recurrence             = "0 09 * * MON-FRI"
}

