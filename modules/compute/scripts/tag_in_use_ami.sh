#!/usr/bin/env bash

# Remove InUse tag from previous AMIs matching ami_name_pattern* and add it to the AMI in use
# The tags tells cleanup scripts in CI not to remove this AMI

# Requires aws commandline

# get old matching ami
ami_old_id=$(aws ec2 describe-images --owner "self" --filters "Name=name,Values=${ami_name_pattern}*" --output json --query "(Images[?(Tags[?Key=='InUse' && Value == 'true'])][].ImageId)[0]")
# need to strip quotes
ami_old_id=$(sed -e 's/^"//' -e 's/"$//' <<< ${ami_old_id} )

if [ ${ami_old_id} != "null" ] ; then
  echo "Removing InUse tag from ${ami_old_id}"
  aws ec2 delete-tags --tags="Key=InUse" --resources ${ami_old_id}
fi

echo "Adding InUse tag to ${ami_in_use_id}"
aws ec2 create-tags --tags="Key=InUse,Value=true" --resources ${ami_in_use_id}