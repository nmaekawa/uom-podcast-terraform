# Compute

Define all instances, target groups and loadbalancers. ECS types and target groups sizes defined as locals,  
see compute/main.tf

## Usage:
```
module "compute" {
  source = "./compute"
  prefix = "podcast-prod"
  env = "prod"
  region = "us-east"
  zones = ["us-east-1", "us-west-1"]
  account = "12345678901"
  key_name = "podcast"
  protocol_external = "${local.protocol_external}"

  vpc_id = "${module.network.vpc_id}"
  subnet_public_ids = "${module.network.subnet_public_ids}"
  subnet_restricted_ids = "${module.network.subnet_restricted_ids}"
  subnet_processing_ids = "${module.network.subnet_processing_ids}"
  subnet_delivery_ids = "${module.network.subnet_delivery_ids}"

  security_group_default_id = "${module.security.security_group_default_id}"
  security_group_users_id = "${module.security.security_group_users_id}"
  security_group_operations_id = "${module.security.security_group_operations_id}"
  profile_publication_name = "${module.security.profile_publication_name}"
  profile_admin_name = "${module.security.profile_admin_name}"

  efs_share_points = "${module.storage.efs_share_points}"
  ebs_opencast_solr_id = "${module.storage.ebs_opencast_solr_id}"
  ebs_opencast_elasticsearch_id = "${module.storage.ebs_opencast_elasticsearch_id}"
  s3_archive_id = "${module.storage.s3_archive_id}"
  glacier_archive_id = "${module.storage.glacier_archive_id}"
  s3_distribution_id = "${module.storage.s3_distribution_id}"* }
```

## Providers

| Name | Version |
|------|---------|
| aws | >= 2.7.0 |
| null | n/a |
| template | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| admin\_standby | Create a standby Opencast Admin instance | `bool` | `false` | no |
| admin\_type | Opecnast Admin instance type | `string` | `"t3.large"` | no |
| autoscale\_upload\_desired | Desired number of Opencast Upload instances during active periods | `number` | `1` | no |
| autoscale\_upload\_max | Maximum number of Opencast Upload instances during active periods | `number` | `2` | no |
| autoscale\_upload\_min | Minimum number of Opencast Upload instances during active periods | `number` | `1` | no |
| autoscale\_upload\_network\_in\_target | Opencast Upload Target netowrk load B/s | `number` | `10000000` | no |
| autoscale\_upload\_quiet\_desired | Desired number of Opencast Upload instances during quiet periods | `number` | `1` | no |
| autoscale\_upload\_quiet\_max | Maximum number of Opencast Upload instances during quiet periods | `number` | `1` | no |
| autoscale\_upload\_quiet\_min | Minimum number of Opencast Upload instances during quiet periods | `number` | `1` | no |
| autoscale\_work\_cpu\_target | Opencast Work instance target CPU load % | `number` | `70` | no |
| autoscale\_work\_desired | Desired number of Opencast Work instances during active periods | `number` | `1` | no |
| autoscale\_work\_max | Maximum number of Opencast Work instances during active periods | `number` | `2` | no |
| autoscale\_work\_min | Minimum number of Opencast Work instances during active periods | `number` | `1` | no |
| autoscale\_work\_quiet\_desired | Desired number of Opencast Work instances during quiet periods | `number` | `1` | no |
| autoscale\_work\_quiet\_max | Maximum number of Opencast Work instances during quiet periods | `number` | `1` | no |
| autoscale\_work\_quiet\_min | Minimum number of Opencast Work instances during quiet periods | `number` | `1` | no |
| ebs\_opencast\_archive\_id | n/a | `any` | n/a | yes |
| ebs\_opencast\_elasticsearch\_id | n/a | `any` | n/a | yes |
| ebs\_opencast\_solr\_id | storage inputs | `any` | n/a | yes |
| edit\_count | Number of Opencast Edit instances (static) | `number` | `1` | no |
| edit\_type | Opencast Edit instance type | `string` | `"t3.small"` | no |
| efs\_share\_points | n/a | `list(string)` | n/a | yes |
| env | Deployed environment | `any` | n/a | yes |
| glacier\_archive\_id | n/a | `any` | n/a | yes |
| log\_type | Log instance type | `string` | `"t3.medium"` | no |
| nat\_type | NAT instance type | `string` | `"t2.micro"` | no |
| network\_interface\_nat\_id | The NAT's network interface ID | `any` | n/a | yes |
| ops\_type | Operation instance type | `string` | `"t3.micro"` | no |
| prefix | Resource naming prefix | `any` | n/a | yes |
| primary\_zone\_index | Primary availability zone | `number` | `0` | no |
| profile\_names | instance profiles | <pre>object({<br>    admin  = string<br>    publication = string<br>    upload = string<br>    work   = string<br>    video  = string<br>    tools  = string<br>    ops    = string<br>  })</pre> | n/a | yes |
| region | AWS region | `any` | n/a | yes |
| s3\_archive\_id | n/a | `any` | n/a | yes |
| s3\_distribution\_id | n/a | `any` | n/a | yes |
| search\_standby | Create a standby Solr index instance | `bool` | `false` | no |
| search\_type | Solr index instance type | `string` | `"t3.small"` | no |
| security\_group\_ids | n/a | <pre>object({<br>    default = string<br>    users = string<br>    nat = string<br>    restricted = string<br>    bastion = string<br>  })</pre> | n/a | yes |
| subnet\_ids | n/a | <pre>object({<br>    public     = list(string)<br>    restricted = list(string)<br>    processing = list(string)<br>    delivery   = list(string)<br>  })</pre> | n/a | yes |
| tools\_standby | Create a standby Podcast Tools instance | `bool` | `false` | no |
| tools\_type | Podcast tools instance type | `string` | `"t3.micro"` | no |
| upload\_type | Opencast Upload instance type | `string` | `"t3.small"` | no |
| video\_count | Number of Openacst Video instances (static) | `number` | `1` | no |
| video\_type | Video Portal instance type | `string` | `"t3.micro"` | no |
| vpc\_cidr | VPC CIDR | `any` | n/a | yes |
| vpc\_id | VPC identity | `any` | n/a | yes |
| work\_type | Opencat Work instance type | `string` | `"t3.medium"` | no |
| zones | List of availability zones to use | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| admin\_ids | n/a |
| admin\_ips | n/a |
| lb\_private\_dns\_name | n/a |
| lb\_private\_zone\_id | n/a |
| lb\_public\_dns\_name | n/a |
| lb\_public\_https\_arn | n/a |
| lb\_public\_zone\_id | n/a |
| lb\_restricted\_dns\_name | n/a |
| lb\_restricted\_https\_arn | n/a |
| lb\_restricted\_zone\_id | n/a |
| log\_ip | n/a |
| log\_ip\_internal | TEMP |
| ops\_ip | n/a |
| search\_ids | n/a |
| search\_ips | n/a |

