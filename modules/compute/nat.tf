# network/nat.tf

# AMI for NAT instances
data "aws_ami" "nat" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn-ami-vpc-nat-hvm-2018*"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["amazon"]
}

resource "aws_instance" "nat" {
  ami           = data.aws_ami.nat.id
  instance_type = var.nat_type
  key_name      = local.key_access_name
  root_block_device {
    delete_on_termination = true
  }
  network_interface {
    network_interface_id = var.network_interface_nat_id
    device_index         = 0
  }
  tags = {
    Name      = "${var.prefix}-nat"
    Env       = var.env
    App       = "nat"
    Component = "nat"
  }
}

# Elastic IP association
data "aws_eip" "nat" {
  tags = {
    Name = "${var.prefix}-nat"
  }
}

resource "aws_eip_association" "nat" {
  allocation_id = data.aws_eip.nat.id
  instance_id = aws_instance.nat.id
  network_interface_id = aws_instance.nat.primary_network_interface_id
}

resource "aws_network_interface_sg_attachment" "nat" {
  security_group_id    = var.security_group_ids.nat
  network_interface_id = aws_instance.nat.primary_network_interface_id
}

