# compute/image.tf

#
# AMI for instances
#
data "aws_ami" "centos" {
  most_recent = true
  filter {
    name   = "name"
    values = ["CentOS Linux 7 * 1804_2*"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["679593333241"] # Centos
}

data "aws_ami" "worker" {
  most_recent = true
  filter {
    name   = "name"
    values = ["${var.prefix}-opencast-worker *"]
  }
  owners = ["self"]
}

data "aws_ami" "upload" {
  most_recent = true
  filter {
    name   = "name"
    values = ["${var.prefix}-opencast-upload *"]
  }
  owners = ["self"]
}

locals {
  work_ami_id   = data.aws_ami.worker.image_id
  upload_ami_id = data.aws_ami.upload.image_id
}

