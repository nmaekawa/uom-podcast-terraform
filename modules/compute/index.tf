# compute/data.tf

#
# Solr Index
#
resource "aws_instance" "search" {
  count         = var.admin_standby ? 2 : 1
  ami           = data.aws_ami.centos.id
  instance_type = var.search_type
  subnet_id     = element(var.subnet_ids.processing, count.index)
  key_name      = local.key_access_name
  root_block_device {
    delete_on_termination = true
  }
  vpc_security_group_ids = [var.security_group_ids.default]
  user_data              = count.index == 0 ? data.template_file.primary.rendered : data.template_file.standby.rendered
  tags = {
    Name = "${var.prefix}-search${count.index == 0 ? "" : "-standby"}"
    Env  = var.env
    App  = "solr"
    Component = "index"
    host = "search"
  }
}

resource "aws_volume_attachment" "search_index" {
  device_name = "/dev/sdb"
  volume_id   = var.ebs_opencast_solr_id
  instance_id = aws_instance.search[var.primary_zone_index].id
}

