# compute/variables.tf

# Standard module variables
variable "prefix" {
  description = "Resource naming prefix"
}

variable "env" {
  description = "Deployed environment"
}

variable "region" {
  description = "AWS region"
}

variable "zones" {
  type        = list(string)
  description = "List of availability zones to use"
}

variable "primary_zone_index" {
  default     = 0
  description = "Primary availability zone"
}

variable "security_group_ids" {
  type = object({
    default = string
    users = string
    nat = string
    restricted = string
    bastion = string
  })
}

# instance profiles
variable "profile_names" {
  type = object({
    admin  = string
    publication = string
    upload = string
    work   = string
    video  = string
    tools  = string
    ops    = string
  })
}

# network inputs
variable "vpc_id" {
  description = "VPC identity"
}

variable "vpc_cidr" {
  description = "VPC CIDR"
}

variable "subnet_ids" {
  type = object({
    public     = list(string)
    restricted = list(string)
    processing = list(string)
    delivery   = list(string)
  })
}

# storage inputs
variable "ebs_opencast_solr_id" {
}

variable "ebs_opencast_elasticsearch_id" {
}

variable "ebs_opencast_archive_id" {
}

variable "efs_share_points" {
  type = list(string)
}

variable "s3_archive_id" {
}

variable "glacier_archive_id" {
}

variable "s3_distribution_id" {
}

#
# configure instances
#

# instance type

# NAT
variable "nat_type" {
  default     = "t2.micro"
  description = "NAT instance type"
}

variable "network_interface_nat_id" {
  description = "The NAT's network interface ID"
}

variable "admin_type" {
  default     = "t3.large"
  description = "Opecnast Admin instance type"
}

variable "edit_type" {
  default     = "t3.small"
  description = "Opencast Edit instance type"
}

variable "upload_type" {
  default     = "t3.small"
  description = "Opencast Upload instance type"
}

variable "work_type" {
  default     = "t3.medium"
  description = "Opencat Work instance type"
}

variable "search_type" {
  default     = "t3.small"
  description = "Solr index instance type"
}

variable "tools_type" {
  default     = "t3.micro"
  description = "Podcast tools instance type"
}

variable "video_type" {
  default     = "t3.micro"
  description = "Video Portal instance type"
}

variable "ops_type" {
  default     = "t3.micro"
  description = "Operation instance type"
}

variable "log_type" {
  default     = "t3.medium"
  description = "Log instance type"
}

# default node numbers
variable "admin_standby" {
  default     = false
  description = "Create a standby Opencast Admin instance"
}

variable "search_standby" {
  default     = false
  description = "Create a standby Solr index instance"
}

variable "tools_count" {
  default     = 1
  description = "Number of Podcast Tools instances (static)"
}

variable "edit_count" {
  default     = 1
  description = "Number of Opencast Edit instances (static)"
}

variable "video_count" {
  default     = 1
  description = "Number of Openacst Video instances (static)"
}

# autoscaling
variable "autoscale_work_max" {
  default     = 2
  description = "Maximum number of Opencast Work instances during active periods"
}

variable "autoscale_work_min" {
  default     = 1
  description = "Minimum number of Opencast Work instances during active periods"
}

variable "autoscale_work_desired" {
  default     = 1
  description = "Desired number of Opencast Work instances during active periods"
}

variable "autoscale_work_quiet_max" {
  default     = 1
  description = "Maximum number of Opencast Work instances during quiet periods"
}

variable "autoscale_work_quiet_min" {
  default     = 1
  description = "Minimum number of Opencast Work instances during quiet periods"
}

variable "autoscale_work_quiet_desired" {
  default     = 1
  description = "Desired number of Opencast Work instances during quiet periods"
}

variable "autoscale_work_cpu_target" {
  default     = 70 # %
  description = "Opencast Work instance target CPU load %"
}

variable "autoscale_upload_max" {
  default     = 2
  description = "Maximum number of Opencast Upload instances during active periods"
}

variable "autoscale_upload_min" {
  default     = 1
  description = "Minimum number of Opencast Upload instances during active periods"
}

variable "autoscale_upload_desired" {
  default     = 1
  description = "Desired number of Opencast Upload instances during active periods"
}

variable "autoscale_upload_quiet_max" {
  default     = 1
  description = "Maximum number of Opencast Upload instances during quiet periods"
}

variable "autoscale_upload_quiet_min" {
  default     = 1
  description = "Minimum number of Opencast Upload instances during quiet periods"
}

variable "autoscale_upload_quiet_desired" {
  default     = 1
  description = "Desired number of Opencast Upload instances during quiet periods"
}

variable "autoscale_upload_network_in_target" {
  default     = 10000000 # bytes
  description = "Opencast Upload Target netowrk load B/s"
}

# Load Balancing
variable "loadbalance_edit_internal_stickiness" {
  default = 3600
  description = "Stickiness duration for Opencast Edit internal load-balancing specifically OAIPMH harvesting"
}

variable "loadbalance_upload_internal_stickiness" {
  default = 7200
  description = "Stickiness duration for Opencast Upload internal load-balancing specifically tool uploads"
}
