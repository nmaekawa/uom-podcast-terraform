/*
 * Domain names and aliases. Uses an existing hosted zone.
 */

locals {
  dns_ttl_instance = 86400 # 1day
  dns_ttl_failover = 300   # 5min
}

locals {
  opencast_health_check_protocol = "HTTP"
  opencast_health_check_path     = "/info/me.json"
}

