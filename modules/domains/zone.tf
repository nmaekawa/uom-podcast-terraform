# domains/zone.tf

data "aws_route53_zone" "podcast" {
  name = var.hosted_zone_name
}

# Split Horizon DNS: resolve internally
resource "aws_route53_zone" "internal" {
  name = "${var.env}.${var.hosted_zone_name}"
  vpc {
    vpc_id = var.vpc_id
  }
  tags = {
    Env = var.env
  }
}

locals {
  zone_id          = data.aws_route53_zone.podcast.id
  internal_zone_id = aws_route53_zone.internal.id
}

