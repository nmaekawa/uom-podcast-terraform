# domain/certificates.tf

# Admin SSL
resource "aws_acm_certificate" "admin" {
  domain_name       = "admin.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "admin_cert_validation" {
  name    = aws_acm_certificate.admin.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.admin.domain_validation_options.0.resource_record_type
  zone_id = local.zone_id
  records = [aws_acm_certificate.admin.domain_validation_options.0.resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "admin" {
  certificate_arn         = aws_acm_certificate.admin.arn
  validation_record_fqdns = [aws_route53_record.admin_cert_validation.fqdn]
}

resource "aws_alb_listener_certificate" "admin" {
  listener_arn = var.lb_restricted_https_arn
  certificate_arn = aws_acm_certificate_validation.admin.certificate_arn
}

# Ingest SSL
resource "aws_acm_certificate" "ingest" {
  domain_name       = "upload.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "ingest_cert_validation" {
  name    = aws_acm_certificate.ingest.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.ingest.domain_validation_options.0.resource_record_type
  zone_id = local.zone_id
  records = [aws_acm_certificate.ingest.domain_validation_options.0.resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "ingest" {
  certificate_arn         = aws_acm_certificate.ingest.arn
  validation_record_fqdns = [aws_route53_record.ingest_cert_validation.fqdn]
}

resource "aws_alb_listener_certificate" "ingest" {
  listener_arn = var.lb_restricted_https_arn
  certificate_arn = aws_acm_certificate_validation.ingest.certificate_arn
}

# Edit SSL
resource "aws_acm_certificate" "edit" {
  domain_name       = "edit.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "edit_cert_validation" {
  name    = aws_acm_certificate.edit.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.edit.domain_validation_options.0.resource_record_type
  zone_id = local.zone_id
  records = [aws_acm_certificate.edit.domain_validation_options.0.resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "edit" {
  certificate_arn         = aws_acm_certificate.edit.arn
  validation_record_fqdns = [aws_route53_record.edit_cert_validation.fqdn]
}

resource "aws_alb_listener_certificate" "edit" {
  listener_arn = var.lb_public_https_arn
  certificate_arn = aws_acm_certificate_validation.edit.certificate_arn
}

# Log SSL
resource "aws_acm_certificate" "log" {
  domain_name       = "log.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "log_cert_validation" {
  name    = aws_acm_certificate.log.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.log.domain_validation_options.0.resource_record_type
  zone_id = local.zone_id
  records = [aws_acm_certificate.log.domain_validation_options.0.resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "log" {
  certificate_arn         = aws_acm_certificate.log.arn
  validation_record_fqdns = [aws_route53_record.log_cert_validation.fqdn]
}

resource "aws_alb_listener_certificate" "log" {
  listener_arn = var.lb_restricted_https_arn
  certificate_arn = aws_acm_certificate_validation.log.certificate_arn
}

# Video SSL
resource "aws_acm_certificate" "video" {
  domain_name       = "video.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "video_cert_validation" {
  name    = aws_acm_certificate.video.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.video.domain_validation_options.0.resource_record_type
  zone_id = local.zone_id
  records = [aws_acm_certificate.video.domain_validation_options.0.resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "video" {
  certificate_arn         = aws_acm_certificate.video.arn
  validation_record_fqdns = [aws_route53_record.video_cert_validation.fqdn]
}

resource "aws_alb_listener_certificate" "video" {
  listener_arn = var.lb_public_https_arn
  certificate_arn = aws_acm_certificate_validation.video.certificate_arn
}

# Tools SSL
resource "aws_acm_certificate" "tools" {
  domain_name       = "tools.${var.env}.${var.hosted_zone_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "tools_cert_validation" {
  name    = aws_acm_certificate.tools.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.tools.domain_validation_options.0.resource_record_type
  zone_id = local.zone_id
  records = [aws_acm_certificate.tools.domain_validation_options.0.resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "tools" {
  certificate_arn         = aws_acm_certificate.tools.arn
  validation_record_fqdns = [aws_route53_record.tools_cert_validation.fqdn]
}

resource "aws_alb_listener_certificate" "tools" {
  listener_arn = var.lb_public_https_arn
  certificate_arn = aws_acm_certificate_validation.tools.certificate_arn
}