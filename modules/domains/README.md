Domain names and aliases. Uses an existing hosted zone.

## Providers

| Name | Version |
|------|---------|
| aws | >= 2.7.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| admin\_ids | Instance ids of primary and optional secondary admin instances | `list(string)` | n/a | yes |
| admin\_ips | Internal IPs of primary and optional secondary admin instances | `list(string)` | n/a | yes |
| admin\_standby | Failover to standby Opencast Admin instance | `bool` | `false` | no |
| database\_dns\_name | Database auto-generated domain name | `any` | n/a | yes |
| env | Deployed environment | `any` | n/a | yes |
| hosted\_zone\_name | Primary hosted zone name | `any` | n/a | yes |
| lb\_private\_dns\_name | Domain name of internal loadbalancer | `any` | n/a | yes |
| lb\_private\_zone\_id | Hosted zone id of private loadbalancer | `any` | n/a | yes |
| lb\_public\_dns\_name | Domain name of public loadbalancer | `any` | n/a | yes |
| lb\_public\_https\_arn | ARN of public loadbalancer HTTPS listener | `any` | n/a | yes |
| lb\_public\_zone\_id | Hosted zone id of public loadbalancer | `any` | n/a | yes |
| lb\_restricted\_dns\_name | Domain name of restricted loadbalancer | `any` | n/a | yes |
| lb\_restricted\_https\_arn | ARN of restricted loadbalancer HTTPS listener | `any` | n/a | yes |
| lb\_restricted\_zone\_id | Hosted zone id of restricted loadbalancer | `any` | n/a | yes |
| log\_ip | IP of logging instance | `any` | n/a | yes |
| log\_ip\_internal | TEMP, internal IP of logging instance | `any` | n/a | yes |
| ops\_ip | IP of operations instance | `any` | n/a | yes |
| prefix | Resource naming prefix | `any` | n/a | yes |
| region | AWS region | `any` | n/a | yes |
| search\_ids | Instance ids of primary and optional secondary search instances | `list(string)` | n/a | yes |
| search\_ips | Internal IPs of primary and optional secondary search instances | `list(string)` | n/a | yes |
| search\_standby | Failover to standby Solr index instance | `bool` | `false` | no |
| vpc\_id | VPC identity | `any` | n/a | yes |

## Outputs

No output.

