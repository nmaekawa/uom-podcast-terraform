# Data

Define all database and index services.

## Usage:
```
module "data" {
  source = "./data"
  prefix = "podcast-stage"
  env = "stage"
  region = "eu-west-1"
  zones = "eu-west-1a, eu-west-1b"

  database_type = "db.t2.micro"
  database_storage_size = 10 # GB
  database_subnet_group = [ "subnet_private_1_id", "subnet_private_2_id"]
}
```

## Providers

| Name | Version |
|------|---------|
| aws | >= 2.7.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| database\_backup\_retention | (Prod only) Number of days to keep backups, 0 to disable | `number` | `7` | no |
| database\_backup\_window | (Prod only) Backup time window in , format HH:MM-HH:MM in UTC. Don't overlap maintenance window | `string` | `"03:10-03:40"` | no |
| database\_maintenance\_window | Maintenance datetime window, format ddd:HH:MM-ddd:HH:MM in UTC. Don't overlap backup window | `string` | `"Sun:02:04-Sun:02:34"` | no |
| database\_password | Primary database user's password | `string` | `"CHANGE_ME"` | no |
| database\_storage\_size | Database storage size in GB | `number` | `10` | no |
| database\_subnet\_group\_ids | List of subnet ids to form a group that can access the database | `list(string)` | n/a | yes |
| database\_type | Database instance type | `string` | `"db.t2.small"` | no |
| database\_username | Primary database user | `string` | `"root"` | no |
| env | Deployed environment | `any` | n/a | yes |
| prefix | Resource naming prefix | `any` | n/a | yes |
| primary\_zone\_index | Primary availability zone | `number` | `0` | no |
| region | AWS region | `any` | n/a | yes |
| zones | List of availability zones to use | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| database\_endpoint | Database endpoint |

