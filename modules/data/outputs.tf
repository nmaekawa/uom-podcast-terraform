# database/outputs.tf

output "database_endpoint" {
  value       = aws_db_instance.podcast.endpoint
  description = "Database endpoint"
}

