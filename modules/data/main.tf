/*
 * # Data
 *
 * Define all database and index services.
 *
 * ## Usage:
 * ```
 * module "data" {
 *   source = "./data"
 *   prefix = "podcast-stage"
 *   env = "stage"
 *   region = "eu-west-1"
 *   zones = "eu-west-1a, eu-west-1b"
 *
 *   database_type = "db.t2.micro"
 *   database_storage_size = 10 # GB
 *   database_subnet_group = [ "subnet_private_1_id", "subnet_private_2_id"]
 * }
 * ```
 */

locals {
  database_engine         = "mariadb"
  database_engine_version = "10.1.34"
  database_family         = "${local.database_engine}10.1"
  database_storage        = "gp2"
  database_timezone       = "Europe/Dublin"
}

