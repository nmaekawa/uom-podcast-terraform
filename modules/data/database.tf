# compute/database.tf

resource "aws_db_subnet_group" "private" {
  name       = "${var.prefix}-private"
  subnet_ids = var.database_subnet_group_ids
  tags = {
    Env = var.env
  }
}

resource "aws_db_parameter_group" "podcast" {
  name   = var.prefix
  family = local.database_family
  parameter {
    name  = "time_zone"
    value = local.database_timezone
  }
  tags = {
    Env       = var.env
    Component = "database"
  }
}

resource "aws_db_instance" "podcast" {
  identifier                = var.prefix
  engine                    = local.database_engine
  engine_version            = local.database_engine_version
  storage_type              = local.database_storage
  allocated_storage         = var.database_storage_size
  storage_encrypted         = true
  instance_class            = var.database_type
  db_subnet_group_name      = aws_db_subnet_group.private.name
  name                      = "podcast_${var.env}"
  username                  = var.database_username
  password                  = var.database_password
  parameter_group_name      = aws_db_parameter_group.podcast.name
  skip_final_snapshot       = var.env == "prod" ? false : true
  final_snapshot_identifier = "${var.prefix}-database-final"
  deletion_protection       = var.env == "prod" ? true : false
  maintenance_window        = var.database_maintenance_window
  backup_retention_period   = var.env == "prod" ? var.database_backup_retention : 0
  backup_window             = var.database_backup_window

  tags = {
    Env       = var.env
    App       = "mysql"
    Component = "database"
    host      = "database"
  }
}

