/**
 * # Network
 *
 * Define VPC, subnets, routing and ACLs. VPC and subnets CIDRS defined as locals see network/main.tf
 *
 * ## Usage:
 * ```
 * module "network" {
 *   source = "./network"
 *   prefix = "podcast-prod"
 *   env = "prod"
 *   zones = ["us-east-1", "us-west-1"]
 *   uni_cidrs = ["100.100.0.0/16"]
 *   resource_cidrs = ["10.200.0.0/16"]
 * }
 * ```
 */

locals {
  # VPC range 10.231.3.0 - 10.231.3.255
  vpc_cidr = "10.231.3.0/24"

  # 16 address subnets
  subnet_public_1_cidr   = "10.231.3.0/28"
  subnet_public_2_cidr   = "10.231.3.16/28"
  subnet_delivery_1_cidr = "10.231.3.32/28"
  subnet_delivery_2_cidr = "10.231.3.48/28"

  # 32 address subnets
  subnet_restricted_1_cidr = "10.231.3.64/27" # needs to be 27 to allow Client VPN associations
  subnet_restricted_2_cidr = "10.231.3.96/27"

  # 64 address subnets
  subnet_processing_1_cidr = "10.231.3.128/26"
  subnet_processing_2_cidr = "10.231.3.192/26"
}

locals {
  subnet_public_cidrs     = [local.subnet_public_1_cidr, local.subnet_public_2_cidr]
  subnet_restricted_cidrs = [local.subnet_restricted_1_cidr, local.subnet_restricted_2_cidr]
  subnet_processing_cidrs = [local.subnet_processing_1_cidr, local.subnet_processing_2_cidr]
  subnet_delivery_cidrs   = [local.subnet_delivery_1_cidr, local.subnet_delivery_2_cidr]
}

