# Network

Define VPC, subnets, routing and ACLs. VPC and subnets CIDRS defined as locals see network/main.tf

## Usage:
```
module "network" {
  source = "./network"
  prefix = "podcast-prod"
  env = "prod"
  zones = ["us-east-1", "us-west-1"]
  uni_cidrs = ["100.100.0.0/16"]
  resource_cidrs = ["10.200.0.0/16"]
}
```

## Providers

| Name | Version |
|------|---------|
| aws | >= 2.7.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| env | Deployed environment | `any` | n/a | yes |
| monitoring\_cidrs | Subnets that will be have access for system/application monitoring | `list(string)` | n/a | yes |
| prefix | Resource naming prefix | `any` | n/a | yes |
| region | AWS region | `any` | n/a | yes |
| resource\_cidrs | University subnets that will be accessed by private subnets | `list(string)` | n/a | yes |
| uni\_cidrs | University subnets that can access restricted subnets | `list(string)` | n/a | yes |
| zones | List of availability zones to use | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| network\_interface\_nat\_id | The NAT's network interface ID |
| subnet\_ids | n/a |
| vpc\_cidr | VPC subnet |
| vpc\_id | VPC identity |

