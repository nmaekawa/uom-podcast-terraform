# network/variables.tf

# Standard module variables
variable "prefix" {
  description = "Resource naming prefix"
}

variable "env" {
  description = "Deployed environment"
}

variable "region" {
  description = "AWS region"
}

variable "zones" {
  type        = list(string)
  description = "List of availability zones to use"
}

# University network variables
variable "uni_cidrs" {
  type        = list(string)
  description = "University subnets that can access restricted subnets"
}

variable "resource_cidrs" {
  type        = list(string)
  description = "University subnets that will be accessed by private subnets"
}

variable "monitoring_cidrs" {
  type        = list(string)
  description = "Subnets that will be have access for system/application monitoring"
}

