# storage/block.tf

# Opencast Solr index
resource "aws_ebs_volume" "opencast_solr" {
  availability_zone = element(var.zones, var.primary_zone_index)
  type              = local.index_type
  size              = var.opencast_solr_size
  tags = {
    Name      = "${var.prefix}-opencast-solr"
    Env       = var.env
    Component = "index"
  }
}

# Opencast elasticsearch index
resource "aws_ebs_volume" "opencast_elasticsearch" {
  availability_zone = element(var.zones, var.primary_zone_index)
  type              = local.index_type
  size              = var.opencast_elasticsearch_size
  tags = {
    Name      = "${var.prefix}-opencast-elasticsearch"
    Env       = var.env
    Component = "index"
  }
}

# Opencast archive
resource "aws_ebs_volume" "opencast_archive" {
  availability_zone = element(var.zones, var.primary_zone_index)
  type              = local.index_type
  size              = var.opencast_archive_size
  tags = {
    Name      = "${var.prefix}-opencast-archive"
    Env       = var.env
    Component = "archive"
  }
}

