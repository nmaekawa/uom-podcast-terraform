# storage/variables.tf

# Standard module variables
variable "prefix" {
  description = "Resource naming prefix"
}

variable "env" {
  description = "Deployed environment"
}

variable "region" {
  description = "AWS region"
}

variable "zones" {
  type        = list(string)
  description = "List of availability zones to use"
}

variable "primary_zone_index" {
  default     = 0
  description = "Index of the primary availability zone to use"
}

variable "account" {
  description = "Account number, for ARN calculation"
}

variable "uni_security_account" {
  description = "University security account number, for ARN calculation"
}

# network inputs
variable "subnet_ids" {
  type = object({
    public     = list(string)
    restricted = list(string)
    processing = list(string)
    delivery   = list(string)
  })
}

# domains for CORS
variable "distribution_cors_domains" {
  type = list(string)
  description ="Domain pattern for CORS to S3 distribution bucket"
}

# storage configuration

# ebs sizes in GB
variable "opencast_solr_size" {
  default     = 10
  description = "Size of Opencast Solr index storage in GB"
}

variable "opencast_elasticsearch_size" {
  default     = 5
  description = "Size of Opencast Elasticsearch storage in GB"
}

variable "opencast_archive_size" {
  default     = 100
  description = "Size of Opencast Archive (hot) storage in GB"
}

# object storage
variable "s3_delivery_transition" {
  default     = 60 # days
  description = "Days before moving published objects to Intelligent Tiering"
}

variable "s3_archive_transition" {
  default     = 7 # days
  description = "Days before moving archived objects to Intelligent Tiering"
}

