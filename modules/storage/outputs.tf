# storage/outputs.tf

# ebs ids
output "ebs_opencast_solr_id" {
  value = aws_ebs_volume.opencast_solr.id
}

output "ebs_opencast_elasticsearch_id" {
  value = aws_ebs_volume.opencast_elasticsearch.id
}

output "ebs_opencast_archive_id" {
  value = aws_ebs_volume.opencast_archive.id
}

# efs mount targets
output "efs_share_points" {
  value = ["${aws_efs_mount_target.private[0].dns_name}:/", "${aws_efs_mount_target.private[1].dns_name}:/"]
}

# s3 ids
output "s3_snapshot_id" {
  value       = aws_s3_bucket.snapshots.id
  description = "S3 EBS snapshot bucket id"
}

output "s3_logs_id" {
  value       = data.aws_s3_bucket.logs.id
  description = "S3 Logs bucket id"
}

output "s3_distribution_id" {
  value       = aws_s3_bucket.distribution.id
  description = "S3 Opencast Distribution bucket id"
}

output "s3_archive_id" {
  value       = aws_s3_bucket.archive.id
  description = "S3 Opencast Archive (warm) bucket id"
}

output "glacier_archive_id" {
  value       = aws_glacier_vault.archive.id
  description = "Glacier Opencast Archive (cold) bucket id"
}

