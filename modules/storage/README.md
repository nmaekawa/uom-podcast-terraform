# Storage

Define block, file and object storage as well as their access policies. EBS type and size define as locals,  
see storage/main.tf

## Usage:
```
module "storage" {
  source = "./storage"
  prefix = "podcast-prod"
  env = "prod"
  region = "us-east"
  zones = ["us-east-1", "us-west-1"]
  account = "12345678901"

  subnet_public_ids = "${module.network.subnet_public_ids}"
  subnet_restricted_ids = "${module.network.subnet_restricted_ids}"
  subnet_processing_ids = "${module.network.subnet_processing_ids}"
  subnet_delivery_ids = "${module.network.subnet_delivery_ids}"
```

## Providers

| Name | Version |
|------|---------|
| aws | >= 2.7.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| account | Account number, for ARN calculation | `any` | n/a | yes |
| env | Deployed environment | `any` | n/a | yes |
| opencast\_archive\_size | Size of Opencast Archive (hot) storage in GB | `number` | `100` | no |
| opencast\_elasticsearch\_size | Size of Opencast Elasticsearch storage in GB | `number` | `5` | no |
| opencast\_solr\_size | Size of Opencast Solr index storage in GB | `number` | `10` | no |
| prefix | Resource naming prefix | `any` | n/a | yes |
| primary\_zone\_index | Index of the primary availability zone to use | `number` | `0` | no |
| region | AWS region | `any` | n/a | yes |
| s3\_archive\_transition | Days before moving archived objects to Intelligent Tiering | `number` | `7` | no |
| s3\_delivery\_transition | Days before moving published objects to Intelligent Tiering | `number` | `60` | no |
| subnet\_ids | network inputs | <pre>object({<br>    public     = list(string)<br>    restricted = list(string)<br>    processing = list(string)<br>    delivery   = list(string)<br>  })</pre> | n/a | yes |
| uni\_security\_account | University security account number, for ARN calculation | `any` | n/a | yes |
| zones | List of availability zones to use | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| ebs\_opencast\_archive\_id | n/a |
| ebs\_opencast\_elasticsearch\_id | n/a |
| ebs\_opencast\_solr\_id | ebs ids |
| efs\_share\_points | efs mount targets |
| glacier\_archive\_id | Glacier Opencast Archive (cold) bucket id |
| s3\_archive\_id | S3 Opencast Archive (warm) bucket id |
| s3\_distribution\_id | S3 Opencast Distribution bucket id |
| s3\_logs\_id | S3 Logs bucket id |
| s3\_snapshot\_id | S3 EBS snapshot bucket id |

