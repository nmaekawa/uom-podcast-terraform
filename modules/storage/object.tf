# storage/block.tf

#
# Distribution
#
resource "aws_s3_bucket" "distribution" {
  bucket        = "${var.prefix}-distribution"
  region        = var.region
  acl           = "private" # This is not the default ACL for objects
  force_destroy = local.s3_delete_contents
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = var.distribution_cors_domains
    max_age_seconds = 3600
  }
  lifecycle_rule {
    enabled = true
    transition {
      days          = var.s3_delivery_transition
      storage_class = "INTELLIGENT_TIERING"
    }
  }
  tags = {
    Name      = "${var.prefix}-distribution"
    Component = "distribution"
    Env       = var.env
  }
}

resource "aws_s3_bucket_policy" "distribution" {
  bucket = aws_s3_bucket.distribution.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PublicReadOnly",
      "Action": "s3:GetObject",
      "Effect": "Allow",
      "Principal": "*",
      "Resource": "arn:aws:s3:::${var.prefix}-distribution/*",
      "Condition": {
        "StringEquals": {
           "s3:ExistingObjectTag/Public": "true"
        }
      }
    }
  ]
}
EOF

}

#
# Archive
#
resource "aws_s3_bucket" "archive" {
  bucket        = "${var.prefix}-archive"
  region        = var.region
  acl           = "private"
  force_destroy = local.s3_delete_contents
  lifecycle_rule {
    enabled = true
    transition {
      days          = var.s3_archive_transition
      storage_class = "INTELLIGENT_TIERING"
    }
  }
  tags = {
    Name      = "${var.prefix}-archive"
    Env       = var.env
    Component = "archive"
  }
}


resource "aws_glacier_vault" "archive" {
  name          = "${var.prefix}-archive"
  access_policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Sid": "FullAccess",
      "Effect": "Allow",
      "Action": "glacier:*",
      "Principal": {
        "AWS": "arn:aws:iam::${var.account}:root"
      },
      "Resource": "arn:aws:glacier:${var.region}:${var.account}:vaults/${var.prefix}-archive"
    }
  ]
}
EOF

  tags = {
    Env       = var.env
    Component = "archive"
  }
}

#
# EBS Snaphots
#
resource "aws_s3_bucket" "snapshots" {
  bucket        = "${var.prefix}-snapshots"
  region        = var.region
  acl           = "private"
  force_destroy = local.s3_delete_contents
  tags = {
    Name      = "${var.prefix}-snapshots"
    Env       = var.env
    Component = "backup"
  }
}

#
# Logs
#
data "aws_s3_bucket" "logs" {
  bucket = "media-technologies-logs"
}

