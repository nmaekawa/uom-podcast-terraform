# security/keys.tf

resource "aws_key_pair" "bastion" {
  key_name   = "${var.prefix}-bastion"
  public_key = file(local.bastion_key_file)
}

resource "aws_key_pair" "ansible" {
  key_name   = "${var.prefix}-access"
  public_key = file(local.access_key_file)
}

