# security/outputs.tf

output "security_group_ids" {
  value = {
    default    = data.aws_security_group.default.id
    users      = aws_security_group.users.id
    nat        = aws_security_group.nat.id
    restricted = aws_security_group.restricted.id
    bastion    = aws_security_group.bastion.id
  }
  description = "Security group ids"
}

output "profile_names" {
  value = {
    admin       = aws_iam_instance_profile.admin.name
    publication = aws_iam_instance_profile.publication.name
    upload      = aws_iam_instance_profile.upload.name
    work        = aws_iam_instance_profile.work.name
    video       = aws_iam_instance_profile.video.name
    tools       = aws_iam_instance_profile.tools.name
    ops         = aws_iam_instance_profile.ops.name
  }
  description = "Names of instance profiles (roles)"
}
