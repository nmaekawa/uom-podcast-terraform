# security/groups.tf

#
# User Facing
#
resource "aws_security_group" "users" {
  name        = "${var.prefix}-users"
  description = "User access"
  vpc_id      = var.vpc_id
  tags = {
    Name = "${var.prefix}-users"
    Env  = var.env
  }
}

resource "aws_security_group_rule" "public_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.users.id
  description       = "HTTP from internet"
}

resource "aws_security_group_rule" "public_https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.users.id
  description       = "HTTPS from internet"
}

resource "aws_security_group_rule" "public_http_ipv6" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  ipv6_cidr_blocks  = ["::/0"]
  security_group_id = aws_security_group.users.id
  description       = "HTTP from internet IPv6"
}

resource "aws_security_group_rule" "public_https_ipv6" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  ipv6_cidr_blocks  = ["::/0"]
  security_group_id = aws_security_group.users.id
  description       = "HTTPS from internet IPv6"
}

#
# NAT
#
resource "aws_security_group" "nat" {
  name        = "${var.prefix}-nat"
  description = "NAT"
  vpc_id      = var.vpc_id
  tags = {
    Name = "${var.prefix}-nat"
    Env  = var.env
  }
}

resource "aws_security_group_rule" "nat_http_in" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = [var.vpc_cidr]
  security_group_id = aws_security_group.nat.id
  description       = "HTTP from VPC"
}

resource "aws_security_group_rule" "nat_https_in" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = [var.vpc_cidr]
  security_group_id = aws_security_group.nat.id
  description       = "HTTPS from VPC"
}

resource "aws_security_group_rule" "nat_ssh_in" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = [var.vpc_cidr]
  security_group_id = aws_security_group.nat.id
  description       = "SSH from VPC"
}

resource "aws_security_group_rule" "nat_smtps_in" {
  type              = "ingress"
  from_port         = 465
  to_port           = 465
  protocol          = "tcp"
  cidr_blocks       = [var.vpc_cidr]
  security_group_id = aws_security_group.nat.id
  description       = "SMTPS from VPC"
}

resource "aws_security_group_rule" "nat_http_out" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.nat.id
  description       = "HTTP to internet"
}

resource "aws_security_group_rule" "nat_https_out" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.nat.id
  description       = "HTTPS to internet"
}

resource "aws_security_group_rule" "nat_ssh_out" {
  type              = "egress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.nat.id
  description       = "SSH to internet"
}

resource "aws_security_group_rule" "nat_smtp_out" {
  type              = "egress"
  from_port         = 465
  to_port           = 465
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.nat.id
  description       = "SMTPS to internet"
}

resource "aws_security_group_rule" "nat_https_out_ipv6" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  ipv6_cidr_blocks  = ["::/0"]
  security_group_id = aws_security_group.nat.id
  description       = "HTTPS to internet IPv6"
}

resource "aws_security_group_rule" "nat_ssh_out_ipv6" {
  type              = "egress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  ipv6_cidr_blocks  = ["::/0"]
  security_group_id = aws_security_group.nat.id
  description       = "SSH to internet IPv6"
}

#
# Restricted
#
resource "aws_security_group" "restricted" {
  name        = "${var.prefix}-restricted"
  description = "Restricted access"
  vpc_id      = var.vpc_id
  tags = {
    Name = "${var.prefix}-restricted"
    Env  = var.env
  }
}

resource "aws_security_group_rule" "restricted_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = var.uni_cidrs
  security_group_id = aws_security_group.restricted.id
  description       = "HTTP from Uni"
}

resource "aws_security_group_rule" "restricted_https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = var.uni_cidrs
  security_group_id = aws_security_group.restricted.id
  description       = "HTTPS from Uni"
}

#
# Bastion
#
resource "aws_security_group" "bastion" {
  name        = "${var.prefix}-bastion"
  description = "Bastion access"
  vpc_id      = var.vpc_id
  tags = {
    Name = "${var.prefix}-operations"
    Env  = var.env
  }
}

resource "aws_security_group_rule" "bastion_ssh" {
  type              = "ingress"
  from_port         = "22"
  to_port           = "22"
  protocol          = "tcp"
  cidr_blocks       = var.uni_cidrs
  security_group_id = aws_security_group.bastion.id
  description       = "SSH from Uni"
}

#
# Internal
#
# The default security_group allows all internal traffic within the VPC
# Need to add it explicitly to resources or else it is removed by Terraform
# To modify the standard rules use the aws_default_security_group resource
data "aws_security_group" "default" {
  vpc_id = var.vpc_id
  name   = "default"
}

