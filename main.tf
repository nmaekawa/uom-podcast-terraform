terraform {
  backend "remote" {
    organization = "MediaTechnologiesUoM"
    workspaces {
      prefix = "podcast-"
    }
  }
}

provider "aws" {
  region  = local.region
  profile = "podcast-dev"
}

data "aws_caller_identity" "current" {
}

module "network" {
  source           = "./modules/network"
  prefix           = "${local.service}-${local.env}"
  env              = local.env
  region           = local.region
  zones            = local.zones
  uni_cidrs        = local.uni_cidrs
  resource_cidrs   = local.resource_cidrs
  monitoring_cidrs = local.monitoring_cidrs
}

module "security" {
  source  = "./modules/security"
  prefix  = "${local.service}-${local.env}"
  env     = local.env
  region  = local.region
  zones   = local.zones
  account = data.aws_caller_identity.current.account_id

  vpc_id         = module.network.vpc_id
  vpc_cidr       = module.network.vpc_cidr
  uni_cidrs      = local.uni_cidrs
  resource_cidrs = local.resource_cidrs
}

module "storage" {
  source               = "./modules/storage"
  prefix               = "${local.service}-${local.env}"
  env                  = local.env
  region               = local.region
  zones                = local.zones
  account              = data.aws_caller_identity.current.account_id
  uni_security_account = data.aws_caller_identity.current.account_id # CHANGE ME
  subnet_ids           = module.network.subnet_ids
  distribution_cors_domains = ["https://video.${local.env}.media.its.manchester.ac.uk"]
}

module "data" {
  source = "./modules/data"
  prefix = "${local.service}-${local.env}"
  env    = local.env
  region = local.region
  zones  = local.zones

  database_subnet_group_ids = concat(
    module.network.subnet_ids.processing,
    module.network.subnet_ids.delivery
  )

  database_password = var.database_password
}

module "compute" {
  source = "./modules/compute"
  prefix = "${local.service}-${local.env}"
  env    = local.env
  region = local.region
  zones  = local.zones

  vpc_id                   = module.network.vpc_id
  vpc_cidr                 = module.network.vpc_cidr
  subnet_ids               = module.network.subnet_ids
  network_interface_nat_id = module.network.network_interface_nat_id

  security_group_ids = module.security.security_group_ids
  profile_names      = module.security.profile_names

  efs_share_points              = module.storage.efs_share_points
  ebs_opencast_solr_id          = module.storage.ebs_opencast_solr_id
  ebs_opencast_elasticsearch_id = module.storage.ebs_opencast_elasticsearch_id
  ebs_opencast_archive_id       = module.storage.ebs_opencast_archive_id
  s3_archive_id                 = module.storage.s3_archive_id
  glacier_archive_id            = module.storage.glacier_archive_id
  s3_distribution_id            = module.storage.s3_distribution_id
}

module "domains" {
  source = "./modules/domains"
  prefix = "${local.service}-${local.env}"
  env    = local.env
  region = local.region

  vpc_id = module.network.vpc_id

  lb_public_https_arn     = module.compute.lb_public_https_arn
  lb_public_dns_name      = module.compute.lb_public_dns_name
  lb_public_zone_id       = module.compute.lb_public_zone_id
  lb_restricted_https_arn = module.compute.lb_restricted_https_arn
  lb_restricted_dns_name  = module.compute.lb_restricted_dns_name
  lb_restricted_zone_id   = module.compute.lb_restricted_zone_id
  lb_private_dns_name     = module.compute.lb_private_dns_name
  lb_private_zone_id      = module.compute.lb_private_zone_id

  database_dns_name = split(":", module.data.database_endpoint)[0]

  ops_ip          = module.compute.ops_ip
  log_ip          = module.compute.log_ip
  log_ip_internal = module.compute.log_ip_internal

  admin_standby  = true # required as length of admin_ips can't be computed
  admin_ips      = module.compute.admin_ips
  admin_ids      = module.compute.admin_ids
  search_standby = true # required as length of admin_ips can't be computed
  search_ips     = module.compute.search_ips
  search_ids     = module.compute.search_ids

  hosted_zone_name = "media.its.manchester.ac.uk."
}

